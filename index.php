<!DOCTYPE html>
<html lang="en">
      <?php  include("blocks/head.php"); ?>
  <body>
      <?php  include("blocks/topmenu.php"); ?>
      <?php  include("blocks/header.php"); ?>
      <?php  include("blocks/slider.php"); ?>
      <?php  include("blocks/search.php"); ?>
      <?php  include("blocks/content.php"); ?>
      <?php  include("blocks/newsletter.php"); ?>
      <?php  include("blocks/footer.php"); ?>
      <?php  include("blocks/script.php"); ?>
  </body>
</html>