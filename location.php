<!DOCTYPE html>
<html lang="en">
    <?php  include("blocks/head.php"); ?>
  <body>
    <?php  include("blocks/topmenu.php"); ?>
    <?php  include("blocks/header.php"); ?>
    <div class="row location">
      <br>  
      <h5>خانه > اماکن تفریحی > امامزاده سیدرکن الدین</h5>
      <br>
      <div class="row location-box">
        <div class="row topbox">
          <div class="col-md-7 leftbox">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <div class="item active">
                  <img src="image/slider-01.JPG">
                  <div class="carousel-caption">
                  </div>
                </div><!-- End Item -->
                <div class="item">
                  <img src="image/slider-01.JPG">
                  <div class="carousel-caption">
                  </div>
                </div><!-- End Item -->
                <div class="item">
                  <img src="image/slider-01.JPG">
                  <div class="carousel-caption">
                  </div>
                </div><!-- End Item -->
                <div class="item">
                  <img src="image/slider-01.JPG">
                  <div class="carousel-caption">
                  </div>
                </div><!-- End Item -->
              </div><!-- End Carousel Inner -->
                <ul class="nav nav-pills nav-justified hidden-xs">
                  <li data-target="#myCarousel" data-slide-to="0"><a href="#"><img src="image/slider-01.JPG"/></a></li>
                  <li data-target="#myCarousel" data-slide-to="1"><a href="#"><img src="image/slider-01.JPG"/></a></li>
                  <li data-target="#myCarousel" data-slide-to="2"><a href="#"><img src="image/slider-01.JPG"/></a></li>
                  <li data-target="#myCarousel" data-slide-to="3"><a href="#"><img src="image/slider-01.JPG"/></a></li>
                  <li data-target="#myCarousel" data-slide-to="3"><a href="#"><img src="image/slider-01.JPG"/></a></li>
                </ul>
            </div>
          </div>
          <div class="col-md-5 rightbox">
              <div class="row textbox">
                  <br>
                  <h4>امامزاده سیدرکن الدین</h4>
                  <p>متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی متن تستی</p>
                  <hr>
                  <ul class="shop">
                      <li><p>هزینه سفر</p></li>
                      <li><p>5500000 ریال</p></li>
                  </ul>
                  <br>
                  <br>
                  <br>
                  <div class="buttonbox">
                      <a href="#"><button type="button" class="btn btn-success">خرید</button></a>
                  </div>
                  <hr>
                  <div class="row">
                      <div class="col-md-5">
                          <div class="col-md-4">
                              <span class="flaticon flaticon-location-pointer"></span>
                          </div>
                          <div class="col-md-8">
                              <h5>یزد کوچه هفتم</h5>
                          </div>
                      </div>
                      <div class="col-md-2">

                      </div>
                      <div class="col-md-5">
                          <div class="col-md-4">
                              <span class="flaticon flaticon-trolley"></span>
                          </div>
                          <div class="col-md-8">
                              <h5>461 خرید</h5>
                          </div>
                      </div>
                  </div>
                  <hr>
                  <div class="row">
                      <div class="col-md-5">
                          <div class="col-md-4">
                              <span class="flaticon flaticon-share"></span>
                          </div>
                          <div class="col-md-8">
                              <h5>اشتراک بادوستان</h5>
                          </div>
                      </div>
                      <div class="col-md-2">
                          
                      </div>
                      <div class="col-md-5">
                          <div class="col-md-3">
                              <a href="#"><span class="flaticon flaticon-twitter-logo-button"></span></a>
                          </div>
                          <div class="col-md-3">
                              <a href="#"><span class="flaticon flaticon-facebook-logo-button"></span></a>
                          </div>
                          <div class="col-md-3">
                              <a href="#"><span class="glyphicon glyphicon-send" aria-hidden="true"></span></a>
                              </div>
                          <div class="col-md-3">
                              <a href="#"><span class="flaticon flaticon-google-plus-logo-button"></span></a>
                          </div>
                      </div>
                  </div><br>
              </div>
          </div>
        </div>
        <div class="row bottombox col-xs-12">
          <div class="col-md-1 col-xs-4">
            <span class="flaticon flaticon-map-location"></span>
          </div>
          <div class="col-md-6 col-xs-8">
            <h4>یزد . کوچهء هفتم . امامزاده سیدرکن الدین</h4>
          </div>
          <div class="col-md-5 col-xs-12">
            <div class="container-fluid">
              <!-- Button trigger modal -->
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-lat='21.03' data-lng='105.85'>
                نمایش روی نقشه
              </button>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">نمایش روی نقشه</h4>
                  </div>
                  <div class="modal-body col-xs-12">
                    <div class="row">
                      <div class="col-md-12 modal_body_map">
                        <div class="location-map" id="location-map">
                          <div style="width: 600px; height: 400px;" id="map_canvas">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d492648.81440094014!2d-73.740421967777!3d40.897138203570115!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x89c2f4f214dc56ef%3A0x505b8ddc076c319d!2s1460+Bronx+River+Ave%2C+Bronx%2C+NY+10472%2C+Birle%C5%9Fik+Devletler!3m2!1d40.8355564!2d-73.87640499999999!5e0!3m2!1str!2str!4v1484062277815" width="100%" height="450" frameborder="0" style="border:0;width: 145%;height: 390px;" allowfullscreen>
                              </iframe>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row details">
          <div class="col-md-7">
            <div class="row right-box">
              <div class="col-md-2"><h5>توضیحات</h5></div>
              <div class="col-md-10"></div>
            </div>
            <br>
            <ul>
              <li><p>این مجموعه با ارزش که نقطه خیابان‌های اصلی شهر است</p></li>
              <li><p>این مجموعه با ارزش که نقطه خیابان‌های اصلی شهر است</p></li>
              <li><p>این مجموعه با ارزش که نقطه خیابان‌های اصلی شهر است</p></li>
              <li><p>این مجموعه با ارزش که نقطه خیابان‌های اصلی شهر است</p></li>
              <li><p>این مجموعه با ارزش که نقطه خیابان‌های اصلی شهر است</p></li>
              <li><p>این مجموعه با ارزش که نقطه خیابان‌های اصلی شهر است</p></li>
              <li><p>این مجموعه با ارزش که نقطه خیابان‌های اصلی شهر است</p></li>
              <li><p>این مجموعه با ارزش که نقطه خیابان‌های اصلی شهر است</p></li>
              <li><p>این مجموعه با ارزش که نقطه خیابان‌های اصلی شهر است</p></li>
              <li><p>این مجموعه با ارزش که نقطه خیابان‌های اصلی شهر است</p></li>
              <li><p>این مجموعه با ارزش که نقطه خیابان‌های اصلی شهر است</p></li>
            </ul>
          </div>
          <div class="col-md-5 col-xs-12">
            <div class="row left-box">
              <div class="col-md-3"><h5>شرایط استفاده</h5></div>
              <div class="col-md-9"></div>
            </div>
            <br>
            <ul>
              <li>
                <div class="row term col-xs-12">
                  <div class="col-md-1 col-xs-2"><span class="flaticon flaticon-business-card-of-a-man-with-contact-info"></span></div>
                  <div class="col-md-11 col-xs-10"><h5>فرصت استفاده از 19 شهریور تا 19 مهر 1397 </h5></div>
                </div>
              </li>
              <br>
              <li>
                <div class="row term col-xs-12">
                  <div class="col-md-1 cl-xs-2"><span class="flaticon flaticon-phone-receiver"></span></div>
                  <div class="col-md-11 col-xs-10"><h5>مراجعه حتما با هماهنگی و رزرو تلفنی</h5></div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <hr>
        <div class="container form">
            <div class="form-box">
              <div class="row text-box">
                <div class="col-md-5"></div>
                <div class="col-md-2">
                    <h2>فرم نظرات</h2>
                </div>
                <div class="col-md-5"></div>
              </div>
              <form action="" method="POST">
                <div class="form-group">
                  <h4>نام:</h4>
                  <input type="text" class="form-control" name="slug" />
                </div>
                <div class="form-group">
                  <h4>ایمیل:</h4>
                  <input type="text" class="form-control" name="title" />
                </div>
                <div class="form-group">
                  <h4>پیام:</h4>
                  <textarea rows="5" class="form-control" name="description" ></textarea>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary">
                      ارسال
                  </button>
                </div>
              </form>
            </div>
        </div>
        <div class="container comment">
          <div class="row">
            <div class="row text-box">
              <div class="col-md-5"></div>
              <div class="col-md-2">
                  <h2 class="page-header">نظرات</h2>
              </div>
              <div class="col-md-5"></div>
            </div>
            <br>
            <section class="comment-list">
              <!-- First Comment -->
              <article class="row">
                <div class="col-md-2 col-sm-2 hidden-xs">
                  <figure class="thumbnail">
                    <img class="img-responsive" src="http://www.tangoflooring.ca/wp-content/uploads/2015/07/user-avatar-placeholder.png" />
                    <figcaption class="text-center">نام کاربری</figcaption>
                  </figure>
                </div>
                <div class="col-md-10 col-sm-10">
                  <div class="panel panel-default arrow left">
                    <div class="panel-body">
                      <header class="text-left">
                        <div class="comment-user"><i class="fa fa-user"></i>دیروز</div>
                        <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>1397 / 6 / 25</time>
                      </header>
                      <div class="comment-post">
                        <p>
                          مسافرت خیلی خوبی بود عااااالی بود
                        </p>
                      </div>
                      <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> پاسخ</a></p>
                    </div>
                  </div>
                </div>
              </article>
              <!-- Second Comment Reply -->
              <article class="row">
                <div class="col-md-2 col-sm-2 col-md-offset-1 col-sm-offset-0 hidden-xs">
                  <figure class="thumbnail">
                    <img class="img-responsive" src="http://www.tangoflooring.ca/wp-content/uploads/2015/07/user-avatar-placeholder.png" />
                    <figcaption class="text-center">نام کاربری</figcaption>
                  </figure>
                </div>
                <div class="col-md-9 col-sm-9">
                  <div class="panel panel-default arrow left">
                    <div class="panel-heading right">پاسخ</div>
                    <div class="panel-body">
                      <header class="text-left">
                        <div class="comment-user"><i class="fa fa-user"></i>دیروز</div>
                        <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>1397 / 6 / 25</time>
                      </header>
                      <div class="comment-post">
                        <p>
                          بله واقعا عالی بود
                        </p>
                      </div>
                      <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> پاسخ</a></p>
                    </div>
                  </div>
                </div>
              </article>
            </section>
          </div>
        </div>      
      </div>
    </div>
    <?php  include("blocks/footer.php"); ?>
    <?php  include("blocks/script.php"); ?>
  </body>
</html>