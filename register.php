<!DOCTYPE html>
<html lang="en">
      <?php  include("blocks/head.php"); ?>
  <body>
      <?php  include("blocks/topmenu.php"); ?>
      <?php  include("blocks/header.php"); ?>
      <div class="row register">
          <br>
          <h5>خانه > ثبت مرکزتفریحی</h5>
          <br>
          <div class="row register-box">
            <div class="row col-md-6 col-xs-12">
              <div class="row text-box">
                <div class="col-md-4">
                  <h4>فرم ثبت نام مجموعه</h4>
                </div>
                <div class="col-md-8"></div>
              </div>
              <br>
              <form action="action_page.php">
                <label for="fname">نام مکان تفریحی:</label>
                <input type="text" id="fname" name="firstname" placeholder="">
                <label for="subject">آدرس:</label>
                <textarea id="subject" name="subject" placeholder="" style="height:80px"></textarea>
                <label for="lname">نام مدیرمجموعه:</label>
                <input type="text" id="lname" name="lastname" placeholder="">
                <label for="fname">تلفن ارتباط مستقیم بامدیریت:</label>
                <input type="text" id="fname" name="firstname" placeholder="">
                <label for="fname">تلفن:</label>
                <input type="text" id="fname" name="firstname" placeholder="">
                <label for="fname">شماره همراه:</label>
                <input type="text" id="fname" name="firstname" placeholder="">
                <label for="fname">ایمیل:</label>
                <input type="text" id="fname" name="firstname" placeholder="">
                <label for="fname">گنجایش:</label>
                <input type="text" id="fname" name="firstname" placeholder="">
                <label for="fname">مهمانپذیری:</label>
                <input type="text" id="fname" name="firstname" placeholder="">
                <label for="country">پارکینگ:</label>
                <select id="country" name="country">
                  <option value="australia">دارد</option>
                  <option value="canada">ندارد</option>
                </select>
                <label for="fname">ساعت کارمجموعه:</label>
                <input type="text" id="fname" name="firstname" placeholder="">
                <label for="fname">نوع خدمات:</label>
                <input type="text" id="fname" name="firstname" placeholder="">
                <label for="fname">درصدتخفیف:</label>
                <input type="text" id="fname" name="firstname" placeholder="">
                <label for="subject">امکانات خاص برای میهمانان وگردشگران:</label>
                <textarea id="subject" name="subject" placeholder="" style="height:80px"></textarea>
                <label for="subject">مکان مجموعه gps:</label>
                <textarea id="subject" name="subject" placeholder="" style="height:80px"></textarea>
                <label for="fname">تفاهم نامه:</label>
                <div class="row letter-box">
                  <p>
                      ماده 1 - موضوع تفاهم نامه 
                      سایت در صدد می باشد تا با بکارگیری شیوه های نوین تبلیغاتی وبا استفاده از الگوی فروشندگی هوشمند (سود کمتر فروش بیشتر) همینطوراستفاده از قدرت خرید گروهی، ترتیبی دهد تا از یک سو برای صاحبان کالا و خدمات و از سوی دیگر برای مشتریان و خریداران صرفه اقتصادی بالایی ایجاد نماید. ( ارائه روشی که در آن خریداروفروشنده به یک اندازه سود می برند) در همین راستا قراردان تبلیغات مجموعه در سایت ومعرفی خدمات آن از یکسو و از سوی دیگر پذیرش رسید فروش خدمات یا کالا طبق تخفیف های مذکور در پیوست موضوع این تفاهم نامه می باشد. 
                      ماده 2 - مدت تفاهم نامه 
                      مدت اعتبار این تفاهم نامه از تاریخ / / لغایت / / می باشد که در صورت موافقت طرفین قابل تمدید می باشد. 
                      ماده 3 - تعهدات سایت 
                      3-1 سایت تعهد می نامید که خدمات مجموعه را به مشتریان خود در حیطه سیاست های خود معرفی نماید. 
                      3-2 سایت متعهد به تسویه حساب با مجموعه در تاریخ های یکم و پانزدهم هر ماه می باشد. 
                      3-3 سایت متعهد است نام و نام خانوادگی و کد خرید و تعداد خرید را توسط پیامک و پنل مدیریت فروشنده به مجموعه ارائه کند. 
                      ماده 4 – تعهدات مجموعه 
                      1-4 مجموعه متعهد است بهای کالا و خدمات را به صورت شفاف به سایت اعلام کند و در صورت تغییر قیمت ها موظف است مراتب را به اطلاع سایت برساند. 
                      2-4 مجموعه متعهد است از تمامی اعضاء یزد تفریح پذیرش به عمل آورد. 
                      3-4 مجموعه متعهد است در صورت ارائه کد تخفیف توسط مشتری، بر اساس تخفیف مکتوب شده با مشتری تسویه حساب نماید. 
                      4-4 به هر دلیل در زمانی که تبلیغات در سایت فعال می باشد مجموعه درخواست برداشته شدن تبلیغات از سایت را کند موظف است مبلغ دویست هزار تومان به عنوان هزینه تبلیغات پرداخت نماید. 
                      5-4 مجموعه متعهد است قبل از تسویه حساب از اعتبار کد تخفیف اطمینان حاصل نماید. 
                      6-4 مجموعه متعهد است تمامی پرسنل خود را در مورد نحوه خدمات سایت و کد تخفیف آموزش دهد و اطلاع رسانی کند. 
                      ماده 5- فسخ 
                      در صورتی که هر کدام از طرفین از تعهدات خود تخلف نماید کلیه دیون و خسارات و تعهدات بصورت کامل تسویه شده و این قرارداد قابل فسخ می باشد.
                  </p>
                </div>
                <input type="submit" value="ارسال">
                <br>
              </form>
              <br>
            </div>
            <div class="row col-md-6 col-xs-12">
              <div class="row text-box">
                <div class="col-md-4">
                  <h4>پیشنهادات</h4>
                </div>
                <div class="col-md-8"></div>
              </div>
              <br>
              <label for="fname">یزدتفریح باهدف ارائه پیشنهادات مفرح</label>
              <div class="row term">
                <p>
                    این گروه طبق تفاهم نامه ای با اداره میراث فرهنگی و صنایع گردشگری استان، این آمادگی را جهت ثبت اطلاعات دقیق اماکن تفریحی و گردشگری استان، همچنین ذکر کلیه خدمات را به تفکیک برای ثبت در وب سایتجمع آوری نماید، از شما صاحبان مشاغل مرتبط و دوستداران بحث گردشگری خواهشمندیم با پیوستن به این اتفاق در پیشبرد اهداف گردشگری هوشمند سازمان میراث فرهنگی، گردشگری وصنایع دستی استان یزد ما را یاری فرمایید .
                </p>
              </div>
            </div>
          </div>
      </div>
      <?php  include("blocks/footer.php"); ?>
      <?php  include("blocks/script.php"); ?>
  </body>
</html>