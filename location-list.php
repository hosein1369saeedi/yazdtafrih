<!DOCTYPE html>
<html lang="en">
        <?php  include("blocks/head.php"); ?>
    <body>
        <?php  include("blocks/topmenu.php"); ?>
        <?php  include("blocks/header.php"); ?>
        <div class="row location-list">
            <br>
            <h5>خانه > لیست اماکن</h5>
            <br>
            <div class="container">
                <div class="row">
                    <div class="well">
                        <div class="row text-box">
                            <div class="col-md-5">
                                <h1 class="text-center">لیست اماکن تفریحی یزد</h1>
                            </div>
                            <div class="col-md-7"></div>
                        </div>
                        <br>
                        <div class="list-group">
                            <a href="#" class="list-group-item">
                                <div class="media col-md-3">
                                    <figure class="pull-left">
                                        <img class="media-object img-rounded img-responsive"  src="image/01-4.jpg" alt="placehold.it/350x250" >
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="list-group-item-heading">امامزاده سیدرکن الدین</h4>
                                    <br>
                                    <p class="list-group-item-text">
                                        بقعه‌ی سید رکن‌الدین محمد قاضی نام دارد. قدمت این اثر تاریخی ایرانی، به قرن هشتم هجری قمری باز گشته و در سال ۱۳۱۴ با شماره ۲۴۶ به ثبت آثار ملی ایران رسیده است. با کجارو همراه باشید تا در مورد تاریخچه و معماری این بنای کهن، اطلاعاتی را در اختیار شما قرار دهیم.
                                    </p>
                                </div>
                                <div class="col-md-3 text-center">
                                    <h2> 14240 <small><img src="image/if_view-01_186381.png"/></small></h2>
                                    <button type="button" class="btn btn-primary btn-lg btn-block">بازدید</button>
                                    <div class="stars">
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star-empty"></span>
                                    </div>
                                    <p> رای 4.5 <small> / </small> 5 </p>
                                </div>
                            </a>
                            <a href="#" class="list-group-item">
                                <div class="media col-md-3">
                                    <figure class="pull-left">
                                        <img class="media-object img-rounded img-responsive" src="image/01-4.jpg" alt="placehold.it/350x250" >
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="list-group-item-heading">امامزاده سیدرکن الدین</h4>
                                    <br>
                                    <p class="list-group-item-text">
                                        بقعه‌ی سید رکن‌الدین محمد قاضی نام دارد. قدمت این اثر تاریخی ایرانی، به قرن هشتم هجری قمری باز گشته و در سال ۱۳۱۴ با شماره ۲۴۶ به ثبت آثار ملی ایران رسیده است. با کجارو همراه باشید تا در مورد تاریخچه و معماری این بنای کهن، اطلاعاتی را در اختیار شما قرار دهیم.
                                    </p>
                                </div>
                                <div class="col-md-3 text-center">
                                    <h2> 12424 <small><img src="image/if_view-01_186381.png"/></small></h2>
                                    <button type="button" class="btn btn-primary btn-lg btn-block">بازدید</button>
                                    <div class="stars">
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star-empty"></span>
                                    </div>
                                    <p> رای 3.9 <small> / </small> 5 </p>
                                </div>
                            </a>
                            <a href="#" class="list-group-item">
                                <div class="media col-md-3">
                                    <figure class="pull-left">
                                        <img class="media-object img-rounded img-responsive" src="image/01-4.jpg" alt="placehold.it/350x250">
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="list-group-item-heading">امامزاده سیدرکن الدین</h4>
                                    <br>
                                    <p class="list-group-item-text">
                                        بقعه‌ی سید رکن‌الدین محمد قاضی نام دارد. قدمت این اثر تاریخی ایرانی، به قرن هشتم هجری قمری باز گشته و در سال ۱۳۱۴ با شماره ۲۴۶ به ثبت آثار ملی ایران رسیده است. با کجارو همراه باشید تا در مورد تاریخچه و معماری این بنای کهن، اطلاعاتی را در اختیار شما قرار دهیم.
                                    </p>
                                </div>
                                <div class="col-md-3 text-center">
                                    <h2> 13540 <small><img src="image/if_view-01_186381.png"/></small></h2>
                                    <button type="button" class="btn btn-primary btn-lg btn-block">بازدید</button>
                                    <div class="stars">
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star-empty"></span>
                                    </div>
                                    <p> رای 4.1 <small> / </small> 5 </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php  include("blocks/footer.php"); ?>
        <?php  include("blocks/script.php"); ?>
    </body>
  </html>