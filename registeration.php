<!DOCTYPE html>
<html lang="en">
        <?php  include("blocks/head.php"); ?>
    <body>
        <?php  include("blocks/topmenu.php"); ?>
        <?php  include("blocks/header.php"); ?>
        <div class="row registeration">
                <div id="signupbox" style=" margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-info">
                        <div class="row panel-heading">
                        <div class="col-md-5"></div>
                        <div class="col-md-2">
                            <div class="panel-title">ثبت نام</div>
                        </div>
                        <div class="col-md-5"></div>
                        </div>  
                        <div class="panel-body" >
                            <form id="signupform" class="form-horizontal" role="form">
                                <div id="signupalert" style="display:none" class="alert alert-danger">
                                    <p>Error:</p>
                                    <span></span>
                                </div>
                                <div class="form-group">
                                    <label for="firstname" class="col-md-3 control-label">نام</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="firstname" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">نام خانوادگی</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="email" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">ایمیل</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="passwd" placeholder="">
                                    </div>
                                </div>  
                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">شماره تماس</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="passwd" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">رمزعبور</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="passwd" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label for="password" class="col-md-3 control-label">تکراررمزعبور</label>
                                        <div class="col-md-9">
                                            <input type="password" class="form-control" name="passwd" placeholder="">
                                        </div>
                                    </div>
                                <!-- <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">مرا به خاطر بسپار</label>
                                    <div class="col-md-9">
                                        <input type="checkbox"  name="is_bought_ticket" placeholder="Confirm Password">
                                    </div>
                                </div>    -->
                                <div class="form-group">
                                    <!-- Button -->                                        
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="btn-signup" type="button" class="btn btn-info btn-block"><i class="icon-hand-right"></i>ثبت نام</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> 

        </div>
        <?php  include("blocks/footer.php"); ?>
        <?php  include("blocks/script.php"); ?>
    </body>
</html>