<!DOCTYPE html>
<html lang="en">
    <?php  include("blocks/head.php"); ?>
  <body>
    <?php  include("blocks/topmenu.php"); ?>
    <?php  include("blocks/header.php"); ?>
    <div class="row contact">
        <br>
        <h5>خانه > تماس باما</h5>
        <br>
        <div class="row contact-box">
            <br>
            <div class="container text">
                <div class="col-md-2"><h2>تماس باما</h2></div>
                <div class="col-md-10"></div>
            </div>
            <br>
            <br>
            <div class="row col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="well well-sm">
                                <form>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name"></label>
                                            <input type="text" class="form-control" id="name" placeholder="نام" required="required" />
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label for="email"></label>
                                            <div class="input-group">
                                                <input type="email" class="form-control" id="email" placeholder="ایمیل" required="required"/>
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <div class="form-group">
                                            <label for="subject">موضوع</label>
                                            <select id="subject" name="subject" class="form-control" required="required">
                                                <option value="na" selected="">انتخاب کنید:</option>
                                                <option value="service">General Customer Service</option>
                                                <option value="suggestions">Suggestions</option>
                                                <option value="product">Product Support</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name"></label>
                                            <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required" placeholder="پیام"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">ارسال پیام</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <form>
                                <br>
                                <legend>راه های ارتباط باما <span class="glyphicon glyphicon-globe"></span></legend>
                                <address>
                                    <br>
                                    <strong>
                                        <a href="#"><span class="flaticon flaticon-twitter-logo-button"></span></a>
                                        <a href="#"><span class="flaticon flaticon-instagram-logo"></span></a>
                                        <a href="#"><span class="flaticon flaticon-facebook-logo-button"></span></a>
                                    </strong>
                                    <br>
                                    <br>
                                    <br>
                                    <abbr title="Phone">شماره تماس:</abbr>
                                    <br>
                                    03538219586-09134441008
                                    <br>
                                    <br>
                                </address>
                                <address>
                                    <strong>ایمیل</strong>
                                    <br>
                                    <a href="mailto:#">info@yazdtafrih.ir</a>
                                </address>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row col-md-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d492648.81440094014!2d-73.740421967777!3d40.897138203570115!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x89c2f4f214dc56ef%3A0x505b8ddc076c319d!2s1460+Bronx+River+Ave%2C+Bronx%2C+NY+10472%2C+Birle%C5%9Fik+Devletler!3m2!1d40.8355564!2d-73.87640499999999!5e0!3m2!1str!2str!4v1484062277815" width="100%" height="450" frameborder="0" allowfullscreen>
                </iframe>
            </div>
        </div>
    </div>
    <?php  include("blocks/footer.php"); ?>
    <?php  include("blocks/script.php"); ?>
  </body>
</html>