<div class="row header">
    <div class="container">
        <ul>
            <li class="box" style="width:12%;">
                <div class="icon">
                    <a href="#"><span class="flaticon flaticon-dish"></span></a>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    پذیرایی <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">رستورانهای سنتی</a></li>
                        <li><a href="#">رستورانهای مدرن</a></li>
                        <!-- <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li> -->
                    </ul>
                </div>
            </li>
            <li class="box" style="width:12%;">
                <div class="icon">
                    <a href="#"><span class="flaticon flaticon-tent"></span></a>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    تفریحی ها <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">پارک</a></li>
                        <li><a href="#">مجموعه های آبی</a></li>
                        <li><a href="#">کمپ های کویری</a></li>
                        <li><a href="#">حیات وحش</a></li>
                        <li><a href="#">کوهستان</a></li>
                        <li><a href="#">ورزش های هوایی</a></li>
                        <li><a href="#">رستوران های گردشگری</a></li>
                        <li><a href="#">تورهای تفریحی</a></li>
                    </ul>
                </div>
            </li>
            <li class="box" style="width:12%;">
                <div class="icon">
                    <a href="#"><span class="flaticon flaticon-teamwork"></span></a>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    رویدادها <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">نمایشگاه ها</a></li>
                        <li><a href="#">جشنواره ها-فستیوال ها</a></li>
                        <!-- <li><a href="#">Action</a></li>
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Action</a></li> -->
                    </ul>
                </div>
            </li>
            <li class="box" style="width:12%;">
                <div class="icon">
                    <a href="#"><span class="flaticon flaticon-jigsaw"></span></a>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    سرگرمی ها <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">سینماها</a></li>
                        <li><a href="#">موزه ها</a></li>
                        <li><a href="#">اماکن تاریخی</a></li>
                        <!-- <li class="divider"></li>
                        <li><a href="#">Separated link</a></li> -->
                    </ul>
                </div>
            </li>
            <li class="box" style="width:12%;">
                <div class="icon">
                    <a href="#"><span class="flaticon flaticon-apartment"></span></a>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    واحدهای اقامتی <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">هتل های4و5ستاره</a></li>
                        <li><a href="#">هتل های2و3ستاره</a></li>
                        <li><a href="#">هتل های سنتی</a></li>
                        <!-- <li class="divider"></li>
                        <li><a href="#">Separated link</a></li> -->
                    </ul>
                </div>
            </li>
            <li class="box" style="width:12%;">
                <div class="icon">
                    <a href="#"><span class="flaticon flaticon-drug"></span></a>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    سلامتی و پزشکی <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>
            </li>
            <li class="box" style="width:12%;">
                <div class="icon">
                    <a href="#"><span class="flaticon flaticon-salon"></span></a>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    زیبایی وآرایشی <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>
            </li>
            <li class="box" style="width:12%;">
                <div class="icon">
                    <a href="#"><span class="flaticon flaticon-trolley"></span></a>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    کالاوبرندها <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>