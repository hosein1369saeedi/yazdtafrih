<div class="footer">
<div class="row top-footer">
    <div class="col-md-4 hidden-xs"></div>
    <div class="row col-md-4">
    <h3>مارادنبال کنیددر:</h3>
        <div class="col-md-2 col-xs-4">
            <a href="#"><span class="flaticon flaticon-twitter-logo-button"></span></a>
        </div>
        <div class="col-md-2 col-xs-4">
            <a href="#"><span class="flaticon flaticon-vk-social-logotype"></span></a>
        </div>
        <div class="col-md-2 col-xs-4">
            <a href="#"><span class="flaticon flaticon-instagram-logo"></span></a>
        </div>
        <div class="col-md-2 col-xs-4">
            <a href="#"><span class="flaticon flaticon-facebook-logo-button"></span></a>
        </div>
        <div class="col-md-2 col-xs-4">
            <a href="#"><span class="flaticon flaticon-google-plus-logo-button"></span></a>
        </div>
        <div class="col-md-2 col-xs-4">
            <a href="#"><span class="flaticon flaticon-social-youtube-circular-button"></span></a>
        </div>
    </div>
    <div class="col-md-4 hidden-xs"></div>
</div>
<div class="row center-footer">
    <div class="container">
    <div class="col-md-8">
        <div class="topbox col-xs-12">
        <br>
        <h4>ایمیل:info@yazdtafrih.ir</h4>
        <h4>تلفن:03538219586-09134441008</h4>
        <h4>آدرس:یزد صفاییه روبروی پمپ بنزین دانشگاه ابتدای بلوار امیرکبیرساختمان گنبدگیتی</h4>
        <br>
        </div>
        <br>
        <div class="bottombox col-xs-12">
        <ul>
            <li><a href="#"><p>درباره یزدتفریح</p></a></li>
            <li><a href="#"><p>ثبت مرکزتفریحی</p></a></li>
            <li><a href="#"><p>ثبت شکایات</p></a></li>
            <li><a href="#"><p>قوانین ومقررات</p></a></li>
            <li><a href="#"><P>حقوق مصرف کننده</P></a></li>
        </ul>
        <br>
        </div>
    </div>
    <div class="col-md-4 col-xs-12">
        <div class="topbox">
        <img src="image/yazdtafrih02.png"/>
        </div>
        <div class="bottombox">
        <div class="col-md-6 leftbox">
            <img src="image/footer-01.png"/>
        </div>
        <div class="col-md-6 rightbox">
            <img src="image/footer-02.png"/>
        </div>
        </div>
    </div>
    </div>
</div>
<div class="row bottom-footer">
    <br>
    <h5>طراحی و پیاده سازی : شرکت طراحی سایت ره وب</h5>
    <br>
</div>
</div>