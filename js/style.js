$('.multi-item-carousel .item').each(function(){
    var next = $(this).next();
    if (!next.length) next = $(this).siblings(':first');
    next.children(':first-child').clone().appendTo($(this));
  });
  $('.multi-item-carousel .item').each(function(){
    var prev = $(this).prev();
    if (!prev.length) prev = $(this).siblings(':last');
    prev.children(':nth-last-child(2)').clone().prependTo($(this));
  });

  $(function() {
    $('#carousel').carouFredSel({
      width: '100%',
      items: {
        visible: 3,
        start: -1
      },
      scroll: {
        items: 1,
        duration: 1000,
        timeoutDuration: 3000
      },
      prev: '#prev',
      next: '#next',
      pagination: {
        container: '#pager',
        deviation: 1
      }
    });
  });

  $(document).ready( function() {
    $('#myCarousel').carousel({
		interval:   4000
	});
	
	var clickEvent = false;
	$('#myCarousel').on('click', '.nav a', function() {
			clickEvent = true;
			$('.nav li').removeClass('active');
			$(this).parent().addClass('active');		
	}).on('slid.bs.carousel', function(e) {
		if(!clickEvent) {
			var count = $('.nav').children().length -1;
			var current = $('.nav li.active');
			current.removeClass('active').next().addClass('active');
			var id = parseInt(current.data('slide-to'));
			if(count == id) {
				$('.nav li').first().addClass('active');	
			}
		}
		clickEvent = false;
	});
});